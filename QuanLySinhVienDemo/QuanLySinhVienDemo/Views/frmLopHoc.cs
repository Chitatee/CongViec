﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuanLySinhVienDemo.Database;
using System.Data.Entity;
namespace QuanLySinhVienDemo.Views
{
    public partial class frmLopHoc : DevExpress.XtraEditors.XtraForm
    {
        dbSinhVien db = new dbSinhVien();
             
        public frmLopHoc()
        {
            InitializeComponent();
        }

        private void frmLopHoc_Load(object sender, EventArgs e)
        {
            db.LopHocs.Load();
            lopHocBindingSource.DataSource = db.LopHocs.Local.ToBindingList();

        }
    }
}