﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace QuanLySinhVienDemo.Views
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnLopHoc_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmLopHoc frmLopHoc = new frmLopHoc();
            frmLopHoc.MdiParent = this;
            frmLopHoc.Show();
        }

        private void btnSinhVien_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmSinhVien frmSinhVien = new frmSinhVien();
            frmSinhVien.MdiParent = this;
            frmSinhVien.Show();
        }
    }
}