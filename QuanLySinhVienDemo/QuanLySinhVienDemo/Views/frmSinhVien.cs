﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuanLySinhVienDemo.Database;
using System.Data.Entity;

namespace QuanLySinhVienDemo.Views
{
    public partial class frmSinhVien : DevExpress.XtraEditors.XtraForm
    {
        dbSinhVien db = new dbSinhVien();
        public frmSinhVien()
        {
            InitializeComponent();
        }

        private void sinhVienGridControl_Click(object sender, EventArgs e)
        {

        }

        private void frmSinhVien_Load(object sender, EventArgs e)
        {
            db.LopHocs.Load();
            lopHocBindingSource.DataSource = db.LopHocs.Local.ToBindingList();
            db.SinhViens.Load();
            sinhVienBindingSource.DataSource = db.SinhViens.Local.ToBindingList();

        }
    }
}