﻿namespace May_tinh_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnC = new System.Windows.Forms.Button();
            this.btnCE = new System.Windows.Forms.Button();
            this.btnone = new System.Windows.Forms.Button();
            this.btntwo = new System.Windows.Forms.Button();
            this.btnfour = new System.Windows.Forms.Button();
            this.btnthree = new System.Windows.Forms.Button();
            this.btnfive = new System.Windows.Forms.Button();
            this.btnsix = new System.Windows.Forms.Button();
            this.btnnine = new System.Windows.Forms.Button();
            this.btnseven = new System.Windows.Forms.Button();
            this.btnback = new System.Windows.Forms.Button();
            this.btnmultiply = new System.Windows.Forms.Button();
            this.btnplus = new System.Windows.Forms.Button();
            this.btnminus = new System.Windows.Forms.Button();
            this.btndevide = new System.Windows.Forms.Button();
            this.btnperiod = new System.Windows.Forms.Button();
            this.btnzero = new System.Windows.Forms.Button();
            this.btnequal = new System.Windows.Forms.Button();
            this.btneight = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnC
            // 
            this.btnC.Location = new System.Drawing.Point(19, 129);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(33, 32);
            this.btnC.TabIndex = 1;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = true;
            // 
            // btnCE
            // 
            this.btnCE.Location = new System.Drawing.Point(101, 129);
            this.btnCE.Name = "btnCE";
            this.btnCE.Size = new System.Drawing.Size(33, 32);
            this.btnCE.TabIndex = 2;
            this.btnCE.Text = "CE";
            this.btnCE.UseVisualStyleBackColor = true;
            // 
            // btnone
            // 
            this.btnone.Location = new System.Drawing.Point(201, 129);
            this.btnone.Name = "btnone";
            this.btnone.Size = new System.Drawing.Size(33, 32);
            this.btnone.TabIndex = 3;
            this.btnone.Text = "1";
            this.btnone.UseVisualStyleBackColor = true;
            // 
            // btntwo
            // 
            this.btntwo.Location = new System.Drawing.Point(281, 130);
            this.btntwo.Name = "btntwo";
            this.btntwo.Size = new System.Drawing.Size(33, 32);
            this.btntwo.TabIndex = 4;
            this.btntwo.Text = "2";
            this.btntwo.UseVisualStyleBackColor = true;
            // 
            // btnfour
            // 
            this.btnfour.Location = new System.Drawing.Point(101, 191);
            this.btnfour.Name = "btnfour";
            this.btnfour.Size = new System.Drawing.Size(33, 32);
            this.btnfour.TabIndex = 5;
            this.btnfour.Text = "4";
            this.btnfour.UseVisualStyleBackColor = true;
            // 
            // btnthree
            // 
            this.btnthree.Location = new System.Drawing.Point(19, 191);
            this.btnthree.Name = "btnthree";
            this.btnthree.Size = new System.Drawing.Size(33, 32);
            this.btnthree.TabIndex = 6;
            this.btnthree.Text = "3";
            this.btnthree.UseVisualStyleBackColor = true;
            // 
            // btnfive
            // 
            this.btnfive.Location = new System.Drawing.Point(201, 191);
            this.btnfive.Name = "btnfive";
            this.btnfive.Size = new System.Drawing.Size(33, 32);
            this.btnfive.TabIndex = 7;
            this.btnfive.Text = "5";
            this.btnfive.UseVisualStyleBackColor = true;
            // 
            // btnsix
            // 
            this.btnsix.Location = new System.Drawing.Point(281, 191);
            this.btnsix.Name = "btnsix";
            this.btnsix.Size = new System.Drawing.Size(33, 32);
            this.btnsix.TabIndex = 8;
            this.btnsix.Text = "6";
            this.btnsix.UseVisualStyleBackColor = true;
            // 
            // btnnine
            // 
            this.btnnine.Location = new System.Drawing.Point(194, 255);
            this.btnnine.Name = "btnnine";
            this.btnnine.Size = new System.Drawing.Size(33, 32);
            this.btnnine.TabIndex = 9;
            this.btnnine.Text = "9";
            this.btnnine.UseVisualStyleBackColor = true;
            // 
            // btnseven
            // 
            this.btnseven.Location = new System.Drawing.Point(19, 255);
            this.btnseven.Name = "btnseven";
            this.btnseven.Size = new System.Drawing.Size(33, 32);
            this.btnseven.TabIndex = 10;
            this.btnseven.Text = "7";
            this.btnseven.UseVisualStyleBackColor = true;
            // 
            // btnback
            // 
            this.btnback.Location = new System.Drawing.Point(281, 257);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(33, 32);
            this.btnback.TabIndex = 11;
            this.btnback.Text = "<--";
            this.btnback.UseVisualStyleBackColor = true;
            // 
            // btnmultiply
            // 
            this.btnmultiply.Location = new System.Drawing.Point(194, 324);
            this.btnmultiply.Name = "btnmultiply";
            this.btnmultiply.Size = new System.Drawing.Size(33, 32);
            this.btnmultiply.TabIndex = 13;
            this.btnmultiply.Text = "*";
            this.btnmultiply.UseVisualStyleBackColor = true;
            // 
            // btnplus
            // 
            this.btnplus.Location = new System.Drawing.Point(19, 324);
            this.btnplus.Name = "btnplus";
            this.btnplus.Size = new System.Drawing.Size(33, 32);
            this.btnplus.TabIndex = 14;
            this.btnplus.Text = "+";
            this.btnplus.UseVisualStyleBackColor = true;
            // 
            // btnminus
            // 
            this.btnminus.Location = new System.Drawing.Point(101, 324);
            this.btnminus.Name = "btnminus";
            this.btnminus.Size = new System.Drawing.Size(33, 32);
            this.btnminus.TabIndex = 15;
            this.btnminus.Text = "-";
            this.btnminus.UseVisualStyleBackColor = true;
            // 
            // btndevide
            // 
            this.btndevide.Location = new System.Drawing.Point(281, 325);
            this.btndevide.Name = "btndevide";
            this.btndevide.Size = new System.Drawing.Size(33, 32);
            this.btndevide.TabIndex = 16;
            this.btndevide.Text = "/";
            this.btndevide.UseVisualStyleBackColor = true;
            // 
            // btnperiod
            // 
            this.btnperiod.Location = new System.Drawing.Point(194, 395);
            this.btnperiod.Name = "btnperiod";
            this.btnperiod.Size = new System.Drawing.Size(33, 32);
            this.btnperiod.TabIndex = 17;
            this.btnperiod.Text = ".";
            this.btnperiod.UseVisualStyleBackColor = true;
            // 
            // btnzero
            // 
            this.btnzero.Location = new System.Drawing.Point(19, 395);
            this.btnzero.Name = "btnzero";
            this.btnzero.Size = new System.Drawing.Size(115, 32);
            this.btnzero.TabIndex = 18;
            this.btnzero.Text = "0";
            this.btnzero.UseVisualStyleBackColor = true;
            // 
            // btnequal
            // 
            this.btnequal.Location = new System.Drawing.Point(281, 395);
            this.btnequal.Name = "btnequal";
            this.btnequal.Size = new System.Drawing.Size(33, 32);
            this.btnequal.TabIndex = 19;
            this.btnequal.Text = "=";
            this.btnequal.UseVisualStyleBackColor = true;
            // 
            // btneight
            // 
            this.btneight.Location = new System.Drawing.Point(101, 255);
            this.btneight.Name = "btneight";
            this.btneight.Size = new System.Drawing.Size(33, 32);
            this.btneight.TabIndex = 20;
            this.btneight.Text = "8";
            this.btneight.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 32);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(274, 20);
            this.textBox1.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 439);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btneight);
            this.Controls.Add(this.btnequal);
            this.Controls.Add(this.btnzero);
            this.Controls.Add(this.btnperiod);
            this.Controls.Add(this.btndevide);
            this.Controls.Add(this.btnminus);
            this.Controls.Add(this.btnplus);
            this.Controls.Add(this.btnmultiply);
            this.Controls.Add(this.btnback);
            this.Controls.Add(this.btnseven);
            this.Controls.Add(this.btnnine);
            this.Controls.Add(this.btnsix);
            this.Controls.Add(this.btnfive);
            this.Controls.Add(this.btnthree);
            this.Controls.Add(this.btnfour);
            this.Controls.Add(this.btntwo);
            this.Controls.Add(this.btnone);
            this.Controls.Add(this.btnCE);
            this.Controls.Add(this.btnC);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button btnCE;
        private System.Windows.Forms.Button btnone;
        private System.Windows.Forms.Button btntwo;
        private System.Windows.Forms.Button btnfour;
        private System.Windows.Forms.Button btnthree;
        private System.Windows.Forms.Button btnfive;
        private System.Windows.Forms.Button btnsix;
        private System.Windows.Forms.Button btnnine;
        private System.Windows.Forms.Button btnseven;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Button btnmultiply;
        private System.Windows.Forms.Button btnplus;
        private System.Windows.Forms.Button btnminus;
        private System.Windows.Forms.Button btndevide;
        private System.Windows.Forms.Button btnperiod;
        private System.Windows.Forms.Button btnzero;
        private System.Windows.Forms.Button btnequal;
        private System.Windows.Forms.Button btneight;
        private System.Windows.Forms.TextBox textBox1;
    }
}

